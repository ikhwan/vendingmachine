package com.vending.machine;

import com.vending.machine.Entity.BarangTerjual;
import com.vending.machine.Entity.JenisBarang;
import com.vending.machine.constant.Ansi;

import java.util.*;

public class VendingMachine {

    protected List<JenisBarang> jenisBarangs = Arrays.asList(
            new JenisBarang( "A", "Biskuit", 5, 0, 0, (double) 6000),
            new JenisBarang( "B", "Chips", 6, 0, 0, (double) 8000),
            new JenisBarang( "C", "Oreo", 7, 0, 0, (double) 10000),
            new JenisBarang( "D", "Tango", 8, 0, 0, (double) 12000),
            new JenisBarang( "E", "Cokelat", 10, 0, 0, (double) 15000)
    );

    protected List<BarangTerjual> barangTerjualList = new ArrayList<>();

    protected BarangTerjual barangTerjual = new BarangTerjual();

    Scanner in = new Scanner(System.in);
    Scanner jumlahDibeli = new Scanner(System.in);
    Scanner uangDiterima = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print( "Vending Machine" );
        VendingMachine run = new VendingMachine();
        run.runApp();
    }

    public void runApp() {
        displayHeaderApp();
        String pilih = in.nextLine();
        switch (pilih) {
            case "1":
                listData();
                runApp();
                break;
            case "2":
                penjualan();
                runApp();
                break;
            default:
                System.out.println( Ansi.ANSI_RED +"Inputan Salah .!"+ Ansi.ANSI_RESET);
                runApp();
                break;
        }
    }

    public void listData() {
        System.out.println( "---------------------------------------------------------" );
        System.out.println( "|Kode |"+"Jenis Makanan |"+"Harga |"+"Terjual |"+"Sisa |");
        System.out.println( "---------------------------------------------------------" );
        if (jenisBarangs.size() > 0) {
            for (JenisBarang data : jenisBarangs) {
                System.out.println("|" + data.getNo() +
                        "   |" + data.getNama() +
                        "       |" + data.getHarga() +
                        "       |" + data.getTerjual() +
                        "       |" + data.getSisaStok()
                );
            }
        }
    }

    public void penjualan() {
        displayHeaderPenjualan();
        System.out.print( "Pilih Kode Barang :");
        String pilih = in.nextLine();
        if (jenisBarangs.size() > 0) {
            JenisBarang data = getJenisMakanan( pilih );
            System.out.println( "---------------------------------------------------------" );
            System.out.print( "Jumlah Barang : ");
            String jumlah = jumlahDibeli.nextLine();
            System.out.print( "Uang diterima : ");
            String uangTerima = uangDiterima.nextLine();
            if (Boolean.TRUE.equals( validateInput( data, uangTerima, jumlah ) )) {
                data = updateJenisBarang( data, jumlah, uangTerima );
                displayPenjualan( data, barangTerjual );
            }
        }
    }

    public Boolean validateInput(JenisBarang data, String jumlah, String uangTerima) {
        String message = "";
        if (uangTerima.equals( "" ) || jumlah.equals( "" )) {
            message = "Uang Diterima atau jumlah Barang harus diisi .!";
        } else {
            if (Double.valueOf(uangTerima) < (data.getHarga() * Double.valueOf(jumlah)) ) {
                message = "Uang yang di terima kurang dari jumlah barang yang di beli : ";
            }
        }
        if (message != "") {
            System.out.println(Ansi.ANSI_RED + "---------------------------------------------------------");
            System.out.println(Ansi.ANSI_RED + message + Ansi.ANSI_RESET);
            penjualan();
            return false;
        }
        return true;
    }

    public JenisBarang updateJenisBarang(JenisBarang data, String jumlah, String uangTerima) {
        data.setTerjual( data.getTerjual() + Integer.parseInt(jumlah) );
        data.setSisaStok( data.getStock() - data.getTerjual() );

        // TODO : set barang terjual
        barangTerjual.setNoJenisBarang( data.getNo() );
        barangTerjual.setUangDiterima( Double.valueOf( uangTerima) );
        barangTerjual.setJumlahTerjual( Integer.parseInt(jumlah) );
        barangTerjual.setUangKembali( Integer.parseInt( uangTerima) - (data.getHarga() * Integer.parseInt( jumlah )) );
        barangTerjual.setTglTerjual( new Date() );
        barangTerjualList.add( barangTerjual );
        return data;
    }

    public JenisBarang getJenisMakanan(String pilih) {
        JenisBarang jenisBarang = null;
        if (pilih.toUpperCase().equals( "a" ) || pilih.toLowerCase().equals( "a" )) {

        }
        switch (pilih) {
            case "A":
                jenisBarang = jenisBarangs.get( 0 );
                break;
            case "B":
                jenisBarang = jenisBarangs.get( 1 );
                break;
            case "C":
                jenisBarang = jenisBarangs.get( 2 );
                break;
            case "D":
                jenisBarang = jenisBarangs.get( 3 );
                break;
            case "E":
                jenisBarang = jenisBarangs.get( 4 );
                break;
            default:
                System.out.println(Ansi.ANSI_RED+ "Pilih Kode sesuai yang di tentukan Guanakan Huruf Kapital"+Ansi.ANSI_RESET);
                penjualan();
                break;
        }
        return jenisBarang;
    }

    public void displayPenjualan(JenisBarang data, BarangTerjual barangTerjual) {
        // TODO : penjualan
        System.out.println( "---------------------------------------------------------" );
        System.out.println( "Jenis makanan : " + data.getNama());
        System.out.println( "Harga : " + data.getHarga());
        System.out.println( "Jumlah Barang dibeli : " + barangTerjual.getJumlahTerjual());
        System.out.println( "Uang diterima : "+ barangTerjual.getUangDiterima());
        System.out.println( "Uang Kembali : "+ barangTerjual.getUangKembali());
        System.out.println( "---------------------------------------------------------" );
    }

    public void displayHeaderApp() {
        System.out.println( "||---------------------------------------------------------||" );
        System.out.println( "Vending Machine");
        System.out.println( "Input 1 : Lihat List data");
        System.out.println( "Input 2 : Penjualan Barang");
        System.out.println( "||---------------------------------------------------------||" );
        System.out.print( "Pilih : ");
    }

    public void displayHeaderPenjualan() {
        System.out.println( "---------------------------------------------------------" );
        System.out.println( "Penjualan Barang");
        System.out.println( "---------------------------------------------------------" );
        System.out.println( "A = Biskuit");
        System.out.println( "B = Chips");
        System.out.println( "C = Oreo");
        System.out.println( "D = Tango");
        System.out.println( "E = Cokelat");
    }
}
