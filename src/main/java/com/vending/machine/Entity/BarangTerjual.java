package com.vending.machine.Entity;

import java.util.Date;

public class BarangTerjual {
    public String id;

    public String noJenisBarang;

    public int jumlahTerjual;

    public Double uangDiterima;

    public Double uangKembali;

    public Date tglTerjual;

    public String getId() {
        return id;
    }

    public int getJumlahTerjual() {
        return jumlahTerjual;
    }

    public void setJumlahTerjual(int jumlahTerjual) {
        this.jumlahTerjual = jumlahTerjual;
    }

    public void setUangKembali(Double uangKembali) {
        this.uangKembali = uangKembali;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNoJenisBarang() {
        return noJenisBarang;
    }

    public void setNoJenisBarang(String noJenisBarang) {
        this.noJenisBarang = noJenisBarang;
    }

    public Double getUangDiterima() {
        return uangDiterima;
    }

    public void setUangDiterima(Double uangDiterima) {
        this.uangDiterima = uangDiterima;
    }

    public Double getUangKembali() {
        return uangKembali;
    }

    public void setUangKembali(double uangKembali) {
        this.uangKembali = uangKembali;
    }

    public Date getTglTerjual() {
        return tglTerjual;
    }

    public void setTglTerjual(Date tglTerjual) {
        this.tglTerjual = tglTerjual;
    }
}
