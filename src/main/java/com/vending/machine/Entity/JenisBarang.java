package com.vending.machine.Entity;

public class JenisBarang {
    public String no;

    public String nama;

    public int stock;

    public int terjual;

    public int sisaStok;

    public Double harga;

    public JenisBarang(String no, String nama, int stock, int terjual, int sisaStok, Double harga) {
        this.no = no;
        this.nama = nama;
        this.stock = stock;
        this.terjual = terjual;
        this.sisaStok = sisaStok;
        this.harga = harga;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getTerjual() {
        return terjual;
    }

    public void setTerjual(int terjual) {
        this.terjual = terjual;
    }

    public int getSisaStok() {
        return sisaStok;
    }

    public void setSisaStok(int sisaStok) {
        this.sisaStok = sisaStok;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }
}
